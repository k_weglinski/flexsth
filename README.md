# FlexSth
HTML, CSS, JS, Framework based on Flex. Allows you to create flexible responsive designs in a flash.

Based with **LESS, GULP, FLEX**

## Basic structuring
### Wrapper

```
<div class="sth">  <!-- flexes here -->  </div>
```

 flex wrapper, contains flex columns

### Columns

```
<div class="sth-1"> </div>
```

means you're going to use a flex element at width 1. So for example:

```
<div class="sth">
    <div class="sth-1"> </div>
    <div class="sth-2"> </div>
</div>
```

 will display 2 boxes, where the first one takes 1/3 of space and second is 2/3

```
<div class="sth">
    <div class="sth-5"> </div>
    <div class="sth-1"> </div>
    <div class="sth-1"> </div>
</div>
```

 will display 3 boxes, where the first one takes 5/7 of space and second and last one will take 1/7 each.  By default it allows us to use classess up to 10.

### Wrapper styles
If you'd like to align items in vertical center just apply 'sth-mid' to wrapper.

```
<div class="sth sth-mid"> </div>
```

If there's need for spacing between columns use 'sth-space'

```
<div class="sth sth-space"> </div>
```

To make inner element equal height use "sth-eq-h"

```
<div class="sth sth-eq-h"> </div>
```

Basic sth wrapper got maximum width of 1170px, if you'd like it to be full screen width use class "sth_full"

```
<div class="sth sth_full"> </div>
```

### Responsivness
You can add class directly to an element that you're willing to make respnsive or the whole wrapper.

```
<div class="sth sth-max-2">
    <div class="sth-1"> </div>
    <div class="sth-1"> </div>
    <div class="sth-1"> </div>
</div>
```

This will make your block 1/2 1/2 1/2 which means that you'll have two boxes equal size and below them one box of 1/2 width.


```
<div class="sth sth-md-2">
    <div class="sth-1"> </div>
    <div class="sth-3"> </div>
</div>
```

 This will make blocks 1/4 and 3/4 for normal view, and those blocks will shrink to 1/2 and 1/2 for medium size.

```
<div class="sth">
    <div class="sth-1 sth-md-2"> </div>
    <div class="sth-1 sth-md-4"> </div>
</div>
```

And this will make form 1/2 and 1/2 columns with 2/6 and 4/6.

## Usage

```
bower install
sudo npm install
sudo npm install -g gulp
```

set the project name inside your gulpfile.js

```
var projectName = "sampleapp";
```

where sampleapp is your project name. It results in names of dist files which will be:

```
 **sampleapp.min.css, sampleapp.min.js, sampleapp-lib.min.js**
```

**_src** is the folder where you put your source data

**_dist** is the folder where gulp puts output and you should link there

## Gulp

```
gulp serve
```

serves your HTML files with browser-sync and automatically refresh on change. Also it does watch for LESS and JS files change and build them.

```
gulp watch
```

watches for changes in whole _src folder

```
gulp less
```

builds only less files

```
gulp js
```

build only js files

```
gulp img
```

runs images compressor for the files inside _src/img/ and puts output to _dist/img.

```
gulp lib
```

builds your JavaScript library based on array in gulpfile.js

```
gulp.task('lib', function () {
    return gulp.src([
        'YOUR ARRAY HERE',
        'SECOND ELEMENT'
    ])
```

files loads one by one (as the array goes, so if library A is based on lib B and needs B to be preloaded before loading A. Simply put Library A as first in array. **Each lib element should be pre minified if you want them to be minified in your lib file** This script just concatenates lib files.

## More data about less mixins
[https://github.com/sth-group/sth-camouflage](https://github.com/sth-group/sth-camouflage)
